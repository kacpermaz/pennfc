//
//  ViewController.swift
//  NFCTest
//
//  Created by Kacper Mazurkiewicz on 11/07/2019.
//  Copyright © 2019 Kacper. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let nfcController = NFCController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func didTapButton(_ sender: Any) {
        nfcController.startScanning()
    }
    
}


//
//  NFCController.swift
//  NFCTest
//
//  Created by Kacper Mazurkiewicz on 11/07/2019.
//  Copyright © 2019 Kacper. All rights reserved.
//

import Foundation
import CoreNFC
import Combine

let MAX_SINGLE_NDEF_READ_SIZE: Int = 255

class NFCController: NSObject {
    
    let queue = OperationQueue()
    
    private var messageCount: UInt8 = 0
    private var isHeaderLinked: UInt8 = 0
    
    private let ndefTagSelect = NSData(bytes: [0x00, 0xA4, 0x04, 0x00, 0x07, 0xD2, 0x76,
                                               0x00, 0x00, 0x85, 0x01, 0x01, 0x00] as [UInt8], length: 13) as Data
    
    private let capabilityContainerSelect = NSData(bytes: [0x00, 0xA4, 0x00, 0x0C, 0x02, 0xE1, 0x03] as [UInt8], length: 7) as Data
    
    private let capabilityContainerRead = NSData(bytes: [0x00, 0xB0, 0x00, 0x00, 0x0F] as [UInt8], length: 5) as Data
    
    private let ndefFileSelect = NSData(bytes: [0x00, 0xA4, 0x00, 0x0C, 0x02, 0xE1, 0x04] as [UInt8], length: 7) as Data
    
    private let commandLog = NSData(bytes: [0x81, 0xE3, 00, 00, 0x2C, 00, 03, 50, 79, 00, 26, 80, 00, 00, 00, 80, 00, 80,
                                            00, 00, 00, 00, 00, 00, 00, 80, 00, 00, 00, 00, 08, 0x4D, 61, 0x6E, 61, 67, 65,
                                            72, 31, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00] as [UInt8], length: 49) as Data
    
    private let ndefReadSize = NSData(bytes: [0x00, 0xB0, 0x00, 0x00, 0x02]  as [UInt8], length: 5) as Data
    
    private let ndefEmptyFile = NSData(bytes: [0x00, 0xD6, 0x00, 0x00, 0x05, 0x00, 0x03, 0xD0, 0x00, 0x00] as [UInt8],
                                       length: 10) as Data
    
    private let phdEmptyRecord = NSData(bytes: [0xD1, 0x03, 0x01, 0x50, 0x48, 0x44, 0x00, 0x90, 0x00] as [UInt8],
                                        length: 9) as Data
    
    private var maxMsgLength: Int? = nil
    
    override init() {
        super.init()
        
        queue.maxConcurrentOperationCount = 1
    }
    
    func startScanning() {
        createReaderSession()
    }
    
    private func createReaderSession() {
        let nfcReader = NFCTagReaderSession(pollingOption: .iso14443,
                                            delegate: self)
        
        nfcReader?.alertMessage = "Hold your phone near the tag"
        nfcReader?.begin()
    }
}

extension NFCController: NFCTagReaderSessionDelegate {
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print(session)
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        guard let first = tags.first else {
            return
        }
        
        if case let NFCTag.iso7816(tag) = first {
            session.connect(to: first) { error in
                tag.readNDEF { (message: NFCNDEFMessage?, error: Error?) in
                    guard let received = message,
                        let firstReceived = received.records.first else {
                            return
                    }
                    
                    for var byte in firstReceived.payload {
                        print(NSData(bytes: &byte, length: 1))
                    }
                    
                    tag.queryNDEFStatus { (_, _, _) in
                        guard let tagSelect = NFCISO7816APDU(data: self.ndefTagSelect),
                            let containerSelect = NFCISO7816APDU(data:self.capabilityContainerSelect),
                            let containerRead = NFCISO7816APDU(data: self.capabilityContainerRead),
                            let ndefFileSelect = NFCISO7816APDU(data: self.ndefFileSelect) else {
                                return
                        }
                        
                        let firstOperation = CommandOperation(tag: tag, command: tagSelect)
                        let secondOperation = CommandOperation(tag: tag, command: containerSelect)
                        let thirdOperation = CommandOperation(tag: tag, command: containerRead)
                        
                        thirdOperation.completionBlock = {
                            guard let response = thirdOperation.responseData?.0 else {
                                return
                            }
                            self.maxMsgLength = (Int(response[5]) * 256 + Int(response[6]))
                        }
                        
                        let fourthOperation = CommandOperation(tag: tag, command: ndefFileSelect)
                        
                        let phdMessage = NSData(bytes: [0xE3, 0x00, 0x00, 0x2C, 0x00, 0x03, 0x50, 0x79, 0x00, 0x26, 0x80, 0x00, 0x00, 0x00, 0x80,
                                                        0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x08, 0x4D,
                                                        0x61, 0x6E, 0x61, 0x67, 0x65, 0x72, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00] as [UInt8], length: 48) as Data
                        
                        let secondMessage = NSData(bytes: [0xE7, 00, 00, 0x16, 0x00, 0x14, 0x80, 0x2C, 0x02, 0x01,
                                                           0x00, 0x0E, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x0D,
                                                           0x1C, 0x00, 0x04, 0x40, 0x0A, 0x00, 0x00] as [UInt8], length: 26) as Data
                        
                        fourthOperation.completionBlock = {
                            self.waitForRequestAndSendResponse(phdMessage: phdMessage, tag: tag) { _ in
                                self.waitForRequestAndSendResponse(phdMessage: secondMessage, tag: tag) { readMessage in
                                    
                                    for byte in readMessage {
                                        print("\(byte)")
                                    }
                                    print(readMessage)
                                }
                            }
                        }
                        fourthOperation.addDependency(thirdOperation)
                        thirdOperation.addDependency(secondOperation)
                        secondOperation.addDependency(firstOperation)
                        
                        self.queue.addOperations([firstOperation, secondOperation,
                                                  thirdOperation, fourthOperation],
                                                 waitUntilFinished: false)
                    }
                }
            }
        }
    }
    
    private func waitForRequestAndSendResponse(phdMessage: Data, tag: NFCISO7816Tag, completion: @escaping (Data) -> Void) {
        self.read(tag: tag) { newData in
            self.penRead(readData: newData) { penReadData in
                self.createWriteMessage(from: phdMessage) { receivedMessage in
                    self.write(tag: tag, phdMessage: receivedMessage, readFinalData: newData, completion: completion)
                }
            }
        }
    }
    
    private func getAPUResponse(mBuf: Data) {
        
        let data = NSData(data: mBuf)
        
        let shortLength = 2
        let intLength = 4
        let startBitPosition = 12
        
        var handle: UInt16 = 0
        var relativeTime: UInt32 = 0
        var eventType: UInt16 = 0
        var length: UInt16 = 0
        
        // Event Report
        data.getBytes(&handle, range: NSRange(location: startBitPosition, length: shortLength))
        data.getBytes(&relativeTime, range: NSRange(location: startBitPosition + 2, length: intLength))
        data.getBytes(&eventType, range: NSRange(location: startBitPosition + 6, length: intLength))
        data.getBytes(&length, range: NSRange(location: startBitPosition + 8, length: intLength))

        // Configuration Report
    }
    
    private func write(tag: NFCISO7816Tag, phdMessage: Data,
                       readFinalData: Data, completion: @escaping (Data) -> Void) {
        
        guard let msgLen = self.maxMsgLength else {
            return
        }
        
        if (phdMessage.count + 7) > msgLen {
            let updateBinaryClear = NSData(bytes: [0x00, 0xD6, 0x00, 0x00, 0x02, 0x00, 0x00] as [UInt8],
                                           length: 7) as Data
            
            var updateBinaryHdr = NSData(bytes: [0x00, 0xD6, 0x00, 0x02, UInt8(phdMessage.count)] as [UInt8],
                                         length: 5) as Data
            
            updateBinaryHdr.append(phdMessage)
            
            let updateBinarySize = NSData(bytes: [0x00, 0xD6, 0x00, 0x00, 0x02,
                                                  UInt8((phdMessage.count + 2) / 256),
                                                  UInt8((phdMessage.count + 2)) ] as [UInt8],
                                          length: 7) as Data
            
            let apdu = NFCISO7816APDU(data: updateBinaryClear)!
            
            tag.sendCommand(apdu: apdu) { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
                print("\(sw1) \(sw2)")
                let apduBinaryMsg = NFCISO7816APDU(data: updateBinaryHdr)!
                tag.sendCommand(apdu: apduBinaryMsg) { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
                    print("\(sw1) \(sw2)")
                    
                    let apduBinarySize = NFCISO7816APDU(data: updateBinarySize)!
                    tag.sendCommand(apdu: apduBinarySize) { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
                        completion(readFinalData)
                    }
                }
            }
        } else {
            var updateBinaryHdr = NSData(bytes: [0x00, 0xD6, 0x00, 0x00, UInt8(phdMessage.count + 2),
            UInt8((phdMessage.count + 2) / 256), UInt8(phdMessage.count + 2)] as [UInt8], length: 7) as Data
            updateBinaryHdr.append(phdMessage)
            
            let msgAPDU = NFCISO7816APDU(data: updateBinaryHdr)!
            tag.sendCommand(apdu: msgAPDU) { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
                
                completion(readFinalData)
            }
        }
    }
    
    private func read(tag: NFCISO7816Tag,
                      completion: @escaping (Data) -> Void) {
        
        let readSize = NFCISO7816APDU(data: self.ndefReadSize)!
        tag.sendCommand(apdu: readSize) { (readData: Data, sw1: UInt8, sw2: UInt8, error) in
            
            let updateEmptyNdef = self.ndefEmptyFile
            var readBinary = NSData(bytes: [0x00, 0xB0, 0x00, 0x00, 0x02]  as [UInt8], length: 5) as Data
            
            var response = readData
            response.append(contentsOf: [sw1, sw2])
            
            if (response.count == 0x04 && response[2] == 0x90 && response[3] == 0x00) {
                let length = Int(response[0]) * 256 + Int(response[1])
                
                if (length > (2 * MAX_SINGLE_NDEF_READ_SIZE)) {
                    print("Failure validating length")
                }
                
                if (length < MAX_SINGLE_NDEF_READ_SIZE) {
                    
                    readBinary[3] = 0x02
                    readBinary[4] = UInt8(length)
                    
                    let readBinaryCommand = NFCISO7816APDU(data: readBinary)!
                    tag.sendCommand(apdu: readBinaryCommand) { (read2: Data, sw1Read2: UInt8, sw2Read2: UInt8, error: Error?) in
                        var addedSw1Sw2BytesArray = read2
                        addedSw1Sw2BytesArray.append(contentsOf: [sw1Read2, sw2Read2])
                        self.updateEmptyNDefReadCommand(previousReadData: addedSw1Sw2BytesArray, data: updateEmptyNdef,
                                                        tag: tag, completion: completion)
                    }
                }
            }
        }
    }
    
    private func createWriteMessage(from apduMessage: Data, completion: @escaping (Data) -> Void) {
        var phdPayload = apduMessage
        phdPayload.insert(0x00 as UInt8, at: 0)
        let phdRecordHeader = getNextPHDRecordHeader()
        phdPayload[0] = phdRecordHeader
        var i = 1
        for byte in apduMessage {
            phdPayload[i] = byte
            i += 1
        }
        
        let recs = createTnfWellKnownPhdRecord(payload: phdPayload)
        completion( recs )
    }
    
    
    private func createTnfWellKnownPhdRecord(payload: Data) -> Data {
        
        var changedFrame = payload
        changedFrame.insert(0xD1 as UInt8, at: 0) // Write binary command
        changedFrame.insert(0x03 as UInt8, at: 1)
        changedFrame.insert(0x31 as UInt8, at: 2)
        changedFrame.insert(0x50 as UInt8, at: 3)
        changedFrame.insert(0x48 as UInt8, at: 4)
        changedFrame.insert(0x44 as UInt8, at: 5)
        
        return changedFrame
    }
    
    private func getNextPHDRecordHeader() -> UInt8 {
        if self.messageCount == 0 {
            messageCount = 1
        } else {
            messageCount += 2
            messageCount = messageCount % 16
        }
        return messageCount | isHeaderLinked
    }
    
    private func penRead(readData: Data, completion: @escaping (Data) -> Void) {
        let message = NFCNDEFMessage(data: readData)!
        
        let records = message.records
        
        for record in records {
            if record.type == "PHD".data(using: .utf8)! {
                self.messageCount = record.payload.first! & 0x0F
                self.isHeaderLinked = record.payload.first! & 0x80
                
                if self.messageCount == 0 && isHeaderLinked == 0 {
                    self.messageCount = 0
                }
                
                if record.payload.count == 1 {
                    return
                } else if record.payload.count > 1 {
                    if record.payload[1] == 0xE2 {
                        isHeaderLinked = 0x80
                    }
                    let result: Data = record.payload.subdata(in: Range(NSRange(location: 1, length: record.payload.count - 1))!)
                    
                    completion(result)
                }
            }
        }
    }
    
    private func updateEmptyNDefReadCommand(previousReadData: Data, data: Data, tag: NFCISO7816Tag,
                                            completion: @escaping (Data) -> Void) {
        let updateEmptyNDEFAPDU = NFCISO7816APDU(data: data)!
        tag.sendCommand(apdu: updateEmptyNDEFAPDU) { (read: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            print(read)
            
            if (previousReadData.advanced(by: 6) == self.phdEmptyRecord.advanced(by: 6))
            && previousReadData.count == self.phdEmptyRecord.count {
                completion(Data())
            } else {
                let newData = previousReadData.subdata(in: Range(NSRange(location: 0,
                                                                         length: previousReadData.count - 2))!)
                completion(newData)
            }
        }
    }
}

class CommandOperation: Operation {
    let tag: NFCISO7816Tag
    let command: NFCISO7816APDU
    
    private var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    private var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    func executing(_ executing: Bool) {
        _executing = executing
    }
    
    func finish(_ finished: Bool) {
        _finished = finished
    }
    
    var responseData: (Data, UInt8, UInt8, Error?)?
    
    init(tag: NFCISO7816Tag, command: NFCISO7816APDU) {
        self.tag = tag
        self.command = command
    }
    
    override func main() {
        guard isCancelled == false else {
            finish(true)
            return
        }
        executing(true)
        sendCommand(tag: self.tag, command: self.command, callback: { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            self.responseData = (data, sw1, sw2, error)
            self.executing(false)
            self.finish(true)
        })
    }
    
    private func sendCommand(tag: NFCISO7816Tag, command: NFCISO7816APDU,
                             callback: @escaping (_ data: Data, _ sw1: UInt8, _ sw2: UInt8, _ error: Error?) -> Void) {
        tag.sendCommand(apdu: command) { (data: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            callback(data, sw1, sw2, error)
        }
    }
    
    private func isValid(sw1: UInt8, sw2: UInt8) -> Bool {
        return sw1 == 0x90 && sw2 == 0x00
    }
}

extension String {
    func toBytes() -> [UInt8] {
        return Array(self.utf8)
    }
}

extension Int {
    func after(_ block: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(self), execute: block)
    }
}
